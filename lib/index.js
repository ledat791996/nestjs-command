#!/usr/bin/env node
"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const yargs_1 = __importDefault(require("yargs"));
const CrudCreateCommand_1 = require("./module/CrudCreateCommand");
yargs_1.default.usage('Usage: $0 <command> [options]')
    .command(new CrudCreateCommand_1.CrudCreateCommand())
    .recommendCommands()
    .demandCommand(1)
    .strict()
    .alias('v', 'version')
    .help('h')
    .alias('h', 'help')
    .argv;
//# sourceMappingURL=index.js.map