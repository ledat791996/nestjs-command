"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CrudCreateCommand = void 0;
const generate_template_files_1 = require("generate-template-files");
const path_1 = __importDefault(require("path"));
const fs_1 = __importDefault(require("fs"));
class CrudCreateCommand {
    constructor() {
        this.command = 'create <path>';
        this.describe = 'Creates a new CRUD Folder.';
    }
    static async serviceTemplate(name) {
        await (0, generate_template_files_1.generateTemplateFiles)([
            {
                option: name,
                defaultCase: generate_template_files_1.CaseConverterEnum.None,
                entry: {
                    folderPath: __dirname + '/../../src/templates/',
                },
                output: {
                    path: './src/resource/__name__',
                    pathAndFileNameDefaultCase: generate_template_files_1.CaseConverterEnum.CamelCase,
                    overwrite: false,
                },
                dynamicReplacers: [
                    {
                        slot: '__name__',
                        slotValue: name,
                    },
                ],
            },
        ]);
    }
    static async createFile(filePath, content, override = true) {
        return new Promise((ok, fail) => {
            if (override === false && fs_1.default.existsSync(filePath))
                return ok();
            fs_1.default.writeFile(filePath, content, (err) => (err ? fail(err) : ok()));
        });
    }
    builder(args) {
        return args.positional('path', {
            type: 'string',
            describe: 'Path of the CRUD Folder',
            demandOption: true,
        });
    }
    handler(args) {
        try {
            const inputPath = 'src/' + args.path;
            const filename = path_1.default.basename(inputPath);
            CrudCreateCommand.serviceTemplate(filename);
        }
        catch (error) {
            console.error('error: ', error);
            process.exit(1);
        }
    }
}
exports.CrudCreateCommand = CrudCreateCommand;
//# sourceMappingURL=CrudCreateCommand.js.map