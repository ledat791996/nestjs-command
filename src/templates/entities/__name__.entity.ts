import { CreateDateColumn, Entity, PrimaryGeneratedColumn, UpdateDateColumn, BeforeInsert, BeforeUpdate } from 'typeorm';
@Entity('__name__(noCase)')
export class __name__(pascalCase)Entity {
  @PrimaryGeneratedColumn()
  id: number;

  @CreateDateColumn({ name: 'created_at' })
  createdAt: Date;

  @UpdateDateColumn({ name: 'update_at' })
  updateAt: Date;

  @BeforeInsert()
  @BeforeUpdate()
  private async deleteCreatedUpdateAt(): Promise<void> {
    delete this.createdAt;
    delete this.updateAt;
  }
}
