import { PartialType } from '@nestjs/mapped-types';
import { Create__name__(pascalCase)Dto } from './create-__name__(camelCase).dto';

export class Update__name__(pascalCase)Dto extends PartialType(Create__name__(pascalCase)Dto) {}
