import { Controller } from '@nestjs/common';
import { __name__(pascalCase)Service } from './__name__(camelCase).service';
import { __name__(pascalCase)Entity } from './entities/__name__(camelCase).entity';
import { Crud } from '@ledat791996/crud';
import { Create__name__(pascalCase)Dto } from './dto/create-__name__(camelCase).dto';
import { Update__name__(pascalCase)Dto } from './dto/update-__name__(camelCase).dto';
@Crud({
  model: {
    type: __name__(pascalCase)Entity,
  },
  routes: {
    only: ['getOneBase', 'updateOneBase', 'createOneBase', 'getManyBase', 'deleteOneBase'],
  },
  dto: {
    create: Create__name__(pascalCase)Dto,
    update: Update__name__(pascalCase)Dto,
  },
})
@Controller('__name__(noCase)')
export class __name__(pascalCase)Controller {
  constructor(public service: __name__(pascalCase)Service) {}
}
