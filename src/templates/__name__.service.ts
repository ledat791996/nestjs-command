import { TypeOrmCrudService } from '@ledat791996/crud-typeorm';
import { Injectable } from '@nestjs/common';
import { __name__(pascalCase)Entity } from './entities/__name__(camelCase).entity';
import { InjectRepository } from '@nestjs/typeorm';
@Injectable()
export class __name__(pascalCase)Service extends TypeOrmCrudService<__name__(pascalCase)Entity> {
  constructor(@InjectRepository(__name__(pascalCase)Entity) repo) {
    super(repo);
  }
}
