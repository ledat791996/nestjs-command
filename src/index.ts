#!/usr/bin/env node
import yargs from 'yargs';
import { CrudCreateCommand } from './module/CrudCreateCommand';

yargs.usage('Usage: $0 <command> [options]')
    .command(new CrudCreateCommand())
    .recommendCommands()
    .demandCommand(1)
    .strict()
    .alias('v', 'version')
    .help('h')
    .alias('h', 'help')
    .argv;
